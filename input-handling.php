<?php

// check if the request method is POST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name']; // get the user name from the form input
    $message = "Hello, $name! Welcome to our website"; // generate the message
    echo $message; // output the message
}

?>

<form method="POST">

    <label for="name">Enter name:</label>
    
    <input type="text" id="name" name="name">

    <button type="submit">Submit</button>
    
</form>